__author__ = 's127504'

import numpy as np
from scipy.optimize import minimize

from openmdao.main.api import Component
from openmdao.lib.datatypes.api import Float, Array, Bool, Int


class SEAMLoads(Component):


    hub_height = Float(iotype='in', units='m', desc='Hub height')
    tsr = Float(iotype='in', units='m', desc='Design tip speed ratio')
    F = Float(iotype='in', desc='Rotor power loss factor')
    wohler_exponent_blade_flap = Float(iotype='in', desc='Wohler Exponent blade flap')
    wohler_exponent_tower = Float(iotype='in', desc='Wohler Exponent tower bottom')
    nSigma4fatFlap = Float(iotype='in', desc='')
    nSigma4fatTower = Float(iotype='in', desc='')
    dLoad_dU_factor_flap = Float(iotype='in', desc='')
    dLoad_dU_factor_tower = Float(iotype='in', desc='')
    lifetime_cycles = Float(iotype='in', desc='')
    blade_edge_dynload_factor_ext = Float(iotype='in', desc='Extreme dynamic edgewise loads factor')
    blade_edge_dynload_factor_fat = Float(iotype='in', desc='Fatigue dynamic edgewise loads factor')

    max_tipspeed = Float(iotype='in', desc='Maximum tip speed')
    n_wsp = Int(iotype='in', desc='Number of wind speed bins')
    min_wsp = Float(0.0, iotype = 'in', units = 'm/s', desc = 'min wind speed')
    max_wsp = Float(iotype = 'in', units = 'm/s', desc = 'max wind speed')
    rotor_diameter = Float(iotype='in', desc='Rotor rotor_diameter')
    rated_power = Float(iotype='in', units='MW', desc='Rated power')
    turbulence_int = Float(iotype='in', desc='Reference turbulence intensity')

    WeibullInput = Bool(True, iotype='in', desc='Flag for Weibull input')
    weibull_C = Float(iotype = 'in', units='m/s', desc = 'Weibull A')
    weibull_k = Float(iotype = 'in', desc='Weibull C')
    project_lifetime = Float(iotype = 'in', desc='Operating years')

    tower_bottom_moment_max = Float(iotype='out', units='kN*m', desc='Max tower bottom moment')
    blade_root_flap_max = Float(iotype='out', units='kN*m', desc='Max blade root flap moment')
    blade_root_edge_max = Float(iotype='out', units='kN*m', desc='Max blade root flap moment')
    blade_root_flap_leq = Float(iotype='out', units='kN*m', desc='Blade root flap lifetime eq. moment')
    blade_root_edge_leq = Float(iotype='out', units='kN*m', desc='Blade root flap lifetime eq. moment')
    tower_bottom_moment_leq = Float(iotype='out', units='kN*m', desc='Tower bottom lifetime eq. moment')

    # 07/09/2015 addition for DriveSE
    max_thrust = Float(iotype='out', units='N', desc='max rotor thrust')

    def execute(self):

        # calculate rotor thrust
        def _power(a, wsp):

            cp = 4. * a * (1.-a)**2 * self.F
            power = 0.5 * 1.225 * cp * (self.rotor_diameter/2.)**2 * np.pi * wsp**3 / 1.e3
            return np.abs(power - self.rated_power * 1.e3)

        thrust = np.zeros(self.n_wsp)
        power = np.zeros(self.n_wsp)
        wsp = np.linspace(self.min_wsp, self.max_wsp, self.n_wsp)

        for i in range(self.n_wsp):
            a = 0.33
            cp = 4. * a * (1.-a)**2 * self.F
            power[i] = 0.5 * 1.225 * cp * (self.rotor_diameter/2.)**2 * np.pi *wsp[i]**3 / 1.e3

            # find a corresponding to rated_power
            if power[i] > self.rated_power*1.e3:
                res = minimize(_power, a, args=(wsp[i],),
                               bounds=[(1.e-6, 0.333)], method='SLSQP', tol=1.e-8)
                a = res['x']
                cp = 4. * a * (1.-a)**2 * self.F
                power[i] = 0.5 * 1.225 * cp * (self.rotor_diameter / 2.)**2 * np.pi * wsp[i]**3 / 1.e3
            ct = a * 4. * (1.-a) * self.F
            thrust[i] = 0.5*ct*1.225*(self.rotor_diameter/2.)**2*np.pi*wsp[i]**2/1.e3
        self.power = power
        self.thrust = thrust

        #Calculation of turbulence sU
        sU = self.turbulence_int * (0.75 * wsp + 5.6)

        #Calculation of thrust loads
        dTdU = np.gradient(np.array([wsp, thrust]).T)[0][:,1]
        MaxdTdU = np.max(dTdU)

        dTdUeff = np.zeros(self.n_wsp)
        dTdUeff[0] = dTdU[0] * self.dLoad_dU_factor_tower
        for i in range(1, self.n_wsp):
            if dTdU[i] > dTdUeff[i-1]:
                dTdUeff[i] = dTdU[i]*self.dLoad_dU_factor_tower
            else:
                dTdUeff[i] = MaxdTdU*self.dLoad_dU_factor_tower

        # Tower loads
        Tmax = thrust + 3. * sU * dTdUeff
        Tmin = thrust - 3. * sU * dTdUeff
        Tmom = thrust * self.hub_height  # not used?
        Tmommax = Tmax * self.hub_height
        Tmommin = Tmin * self.hub_height
        tower_bottom_moment_max = np.max([np.abs(Tmommax), np.abs(Tmommin)])
        self.tower_bottom_moment_max = tower_bottom_moment_max
        # Calculation of flap loads
        Flap = 2/3.*self.rotor_diameter/2.*1/3.*thrust

        dFdU = np.gradient(np.array([wsp, Flap]).T)[0][:,1]
        MaxdFdU = np.max(dFdU)

        dFdUeff = np.zeros(self.n_wsp)
        dFdUeff[i] = dFdU[1]*self.dLoad_dU_factor_flap
        for i in range(1, self.n_wsp):
            if dFdU[i] > dFdUeff[i-1]:
                dFdUeff[i] = dFdU[i]*self.dLoad_dU_factor_flap
            else:
                dFdUeff[i] = MaxdFdU*self.dLoad_dU_factor_flap

        Fmax = Flap + 3 * sU * dFdUeff
        Fmin = Flap - 3 * sU * dFdUeff
        FlapR = Fmax - Fmin
        blade_root_flap_max = np.max([abs(Fmax), np.abs(Fmin)])
        self.blade_root_flap_max = blade_root_flap_max

        # Fatigue
        if not self.WeibullInput:
            WeibullC = 2.0
            WeibullA = 1.1284 * self.MeanWSP #TODO: where do I come from?
        else:
            WeibullC = self.weibull_k
            WeibullA = self.weibull_C

        Nhours = np.zeros(self.n_wsp)
        Nhours[0] = self.project_lifetime * 8760 * (1 - np.exp(-np.exp(WeibullC*np.log((wsp[0]+0.5)/WeibullA))))
        for i in range(1, self.n_wsp):
            Nhours[i] = self.project_lifetime * 8760 * ((1-np.exp(-np.exp(WeibullC*np.log((wsp[i]+0.5)/WeibullA)))) \
                                            - (1-np.exp(-np.exp(WeibullC*np.log((wsp[i]-0.5)/WeibullA)))) )

        # Flap fatigue
        n1p10min = np.zeros(self.n_wsp)
        for i in range(self.n_wsp):
            if wsp[i] > 0:
                if wsp[i] * self.tsr < self.max_tipspeed:
                    n1p10min[i] = 600. / (2. * np.pi / ((wsp[i] * self.tsr) / (self.rotor_diameter/2)))
                else:
                    n1p10min[i] = 600. / (2. * np.pi / ((self.max_tipspeed)/(self.rotor_diameter/2)))
            else:
                n1p10min[i] = 0

        nSigma4fat = np.zeros(self.n_wsp)
        FlapStel = np.zeros(self.n_wsp)
        for i in range(self.n_wsp):
            nSigma4fat[i] = self.nSigma4fatFlap * sU[i] * dFdUeff[i]
            FlapStel[i] = np.exp(self.wohler_exponent_blade_flap*np.log(2 * nSigma4fat[i])) * (n1p10min[i]/600)
            if FlapStel[i] > 0:
                FlapStel[i] = np.exp((1/self.wohler_exponent_blade_flap)*np.log(FlapStel[i]))
            else:
                FlapStel[i] = 0.

        blade_root_flap_leq = 0.
        FlapLifetime = np.zeros(self.n_wsp)
        for i in range(self.n_wsp):
            if FlapStel[i] > 0:
                FlapLifetime[i] = Nhours[i] * np.exp(self.wohler_exponent_blade_flap * np.log(FlapStel[i])) * 3600
            else:
                FlapLifetime[i] = 0.
            blade_root_flap_leq += FlapLifetime[i]

        blade_root_flap_leq = blade_root_flap_leq / self.lifetime_cycles
        blade_root_flap_leq = np.exp((1. / self.wohler_exponent_blade_flap) * np.log(blade_root_flap_leq))
        self.blade_root_flap_leq = blade_root_flap_leq

        # Tower fatigue
        nSigma4fat = np.zeros(self.n_wsp)
        TowerStel = np.zeros(self.n_wsp)
        for i in range(self.n_wsp):
            nSigma4fat[i] = self.nSigma4fatTower * sU[i] * dTdUeff[i] * self.hub_height
            TowerStel[i] = np.exp(self.wohler_exponent_tower * np.log(2*nSigma4fat[i]))*(n1p10min[i]/600)
            if TowerStel[i] > 0.:
                TowerStel[i] = np.exp((1/self.wohler_exponent_tower)*np.log(TowerStel[i]))
            else:
                TowerStel[i] = 0.

        TowerLifetime = np.zeros(self.n_wsp)
        tower_bottom_moment_leq = 0.
        for i in range(self.n_wsp):
            if TowerStel[i] > 0.:
                TowerLifetime[i] = Nhours[i] * np.exp(self.wohler_exponent_tower * np.log(TowerStel[i]))*3600
            else:
                TowerLifetime[i] = 0.
            tower_bottom_moment_leq += TowerLifetime[i]

        tower_bottom_moment_leq = tower_bottom_moment_leq / self.lifetime_cycles
        tower_bottom_moment_leq = np.exp((1. / self.wohler_exponent_tower) * np.log(tower_bottom_moment_leq))
        self.tower_bottom_moment_leq = tower_bottom_moment_leq

        # Edgewise loads
        SimpleBladeWeight = np.exp(2.0633*np.log(self.rotor_diameter)) * 8.4923 * 0.1
        blade_root_edge_max = (self.rotor_diameter / 2.) * (1./3.) * 9.81 * SimpleBladeWeight * self.blade_edge_dynload_factor_ext / 1000
        self.blade_root_edge_max = blade_root_edge_max
        EdgeSTEL = 2. * blade_root_edge_max * self.blade_edge_dynload_factor_fat / self.blade_edge_dynload_factor_ext
        blade_root_edge_leq = 0
        EdgeLifetime = np.zeros(self.n_wsp)
        for i in range(self.n_wsp):
            if EdgeSTEL > 0.:
                EdgeLifetime[i] = Nhours[i] * np.exp(self.wohler_exponent_blade_flap * np.log(EdgeSTEL)) * 3600
            else:
                EdgeLifetime[i] = 0.
            blade_root_edge_leq += EdgeLifetime[i];

        blade_root_edge_leq = blade_root_edge_leq / self.lifetime_cycles
        blade_root_edge_leq = np.exp((1. / self.wohler_exponent_blade_flap) * np.log(blade_root_edge_leq))
        self.blade_root_edge_leq = blade_root_edge_leq


        print 'blade_root_flap_max = ', blade_root_flap_max
        print 'blade_root_edge_max = ', blade_root_edge_max
        print 'tower_bottom_moment_max = ', tower_bottom_moment_max
        print 'blade_root_flap_leq', blade_root_flap_leq
        print 'blade_root_edge_leq', blade_root_edge_leq
        print 'tower_bottom_moment_leq', tower_bottom_moment_leq

        # additions for nacelle 07/09/2015
        self.max_thrust = thrust.max() * 1000.

if __name__ == '__main__':

    top = SEAMLoads()

    top.hub_height = 100.0

    top.tsr = 8.0
    top.F = 0.777
    top.wohler_exponent_blade_flap = 10.0
    top.wohler_exponent_tower = 4.
    top.nSigma4fatFlap = 1.2
    top.nSigma4fatTower = 0.8
    top.dLoad_dU_factor_flap = 0.9
    top.dLoad_dU_factor_tower = 0.8
    top.lifetime_cycles = 1.0e07
    top.blade_edge_dynload_factor_ext = 2.5
    top.blade_edge_dynload_factor_fat = 0.75

    top.n_wsp = 26
    top.min_wsp = 0.
    top.max_wsp = 25.
    top.rotor_diameter = 101.0
    top.rated_power = 3. #MW
    top.turbulence_int = 0.16
    top.max_tipspeed = 62.

    top.WeibullInput = True
    top.weibull_C = 11.
    top.weibull_k = 2.00
    top.project_lifetime = 20.
    top.execute()
